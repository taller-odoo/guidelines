# Estructura de proyecto:

La raiz del proyecto debe contener obligatoriamente los siguientes archivos:

## Archivos para calidad de código

```
.editorconfig
.gitignore
.gitlab-ci.yml
requirements_test.txt
setup.cfg
```

Que se encuentran en:
https://git.minsa.gob.pe/oidt/odoo-template/tree/master/%7B%7Bcookiecutter.project_slug%7D%7D

## Caso 1 - Proyecto con un único addon

Asumiendo que la carpeta del proyecto se llama `mimodulo`

### 1.1 Addon sencillo (poco contenido)

Su contenido debe ser similar a:

```
.editorconfig
.gitignore
.gitlab-ci.yml
requirements_test.txt
setup.cfg
__init__.py
__manifest__.py
models.py
views.xml
security/security.xml
security/ir.model.access.csv
```


### 1.2 Addon complejo

Su contenido debe ser similar a:

```
.editorconfig
.gitignore
.gitlab-ci.yml
requirements_test.txt
setup.cfg

__init__.py
__manifest__.py
models/
models/mimodulo.py
views/
views/mimodulo_views.xml
security/security.xml
security/ir.model.access.csv
```

Si se hereda de otros addons seguir el patron:

```
models/res_users.py (En caso de herardar res.users)
models/res_partner.py (En caso de herardar res.partner)
models/sale.py (En caso de herardar sale), y para las vistas:
views/res_users_views.xml (En caso de herardar res.users)
models/res_partner_views.xml (En caso de herardar res.partner)
models/sale_views.xml (En caso de herardar sale)
```


## Caso 2 - Proyecto con mas de un addon

Su contenido debe ser similar a:

```
.editorconfig
.gitignore
.gitlab-ci.yml
requirements_test.txt
setup.cfg
addon_1
addon_2
addon_n
```

Y el contenido de cada addon análogamente a lo indicado en `Caso 1 - Proyecto con un único addon`



# Script manifest.py


```python
# -*- coding: utf-8 -*-

{
    'name': "nombre del addon",

    'summary': """
        Módulo para la gestión de pacientes, medicos, citas""",

    'description': """
    """,

    'author': "Minsa",
    'website': "www.minsa.gob.pe",

    'category': 'categoria',
    'version': '0.1',

    'depends': [
        'dependencia1',
        'dependencia2',
        'dependencia3',
    ],
    'external_dependencies': {
        'python': [
        ]
    },
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv.xml',
        'views/mimodulo_views.xml',
        'views/res_partner_views.xml',
        'data/archivo_data1.xml',
        'data/archivo_data2.xml',
    ],
    'application': True,
}

```
