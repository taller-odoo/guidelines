# Modelos:


# Declaración de modelos

Asumiendo que el addon se llama `mimodulo` y el modelo `mimodelo`:

```python
# -*- coding: utf-8 -*-

from odoo import api, fields, models


class MiModelo(models.Model):
    _name = 'mimodulo.mimodelo'
    _description = u'Descripción del modelo'
    
    # Aqui declaración de atributos

    # Aqui declaración de _sql_constrains y métodos que usan el decorador api.constrains
    
    # Aquí declaración de metodos _onchange_atributo api.onchange
    
    # otros métodos
    
```


# Declaración atributos simples

```python

class MiModelo(models.Model):

    code = fields.Char(
        string=u'Código',
        required=True
    )
    name = fields.Char(
        string=u'Nombre',
        required=True,
    )
    identificacion = fields.Integer(string=u'Identificación')
    active = fields.Boolean(string=u'Activo')

```

O:

```python

class MiModelo(models.Model):

    code = fields.Char(
        u'Código',
        required=True
    )
    name = fields.Char(
        'Nombre',
        required=True,
    )
    identificacion = fields.Integer(u'Identificación')
    active = fields.Boolean(string=u'Activo')

```

# Declaración atributos computados

```python

class MiModelo(models.Model):

    total_ingresos = fields.Float(
        string=u'Total Ingresos',
        compute='_compute_total_ingresos',
    )

    @api.depends('atributo1', 'atributo2', )
    def _compute_total_ingresos(self):
        # Declarar aquí las variables iniciales o constantes
        model_partner = self.env['res.partner']
        model_sale_order = self.env['sale.order']

        for record in self:
            #
            # Calculo de total_ingresos
            #
            record.total_ingresos = total_ingresos

```

# Declaración atributos Many2one y One2many

## Many2one

```python

class Item(models.Model):
    _name = 'mimodulo.factura.item'

    factura_id = fields.One2many(
        comodel_name='mimodulo.factura',
        string='Factura',
    )

    # o

    factura_id = fields.One2many(
        'mimodulo.factura',
        'Factura',
    )

```

## One2many

```python

class Factura(models.Model):
    _name = 'mimodulo.factura'

    item_ids = fields.One2many(
        comodel_name='mimodulo.factura.item',
        inverse_name='factura_id',
        string='Items de factura',
    )

    # o

    item_ids = fields.One2many(
        'mimodulo.factura.item',
        'factura_id',
        'Items de factura',
    )

```

# Declaración atributos Many2many

```python

class Hospital(models.Model):
    _name = 'mimodulo.hospital'

    medico_ids = fields.Many2many(
        comodel_name='mimodulo.medico',
        relation='mimodulo_hospital_medico_rel',
        column1='hospital_id',
        column2='medico_id',
        string=u'Médicos'
    )

class Medico(models.Model):
    _name = 'mimodulo.medico'

```


# Declaración de onchange

```python

class MiModelo(models.Model):

    total_ingresos = fields.Float(
        string=u'Total Ingresos',
    )

    @api.onchange('total_ingresos')
    def _onchange_total_ingresos(self):
        #
        # Calculo de nuevo_valor
        #
        self.otra_variable = nuevo_valor

```